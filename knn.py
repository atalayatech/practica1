import numpy as np


class Knn:
    def __init__(self, n_neighbors, metric='minkowski', p=2, weights='uniform'):
        self.n_neighbors = n_neighbors
        # More code here

    def fit(self, X, y):
        # More code here
        self.X = X
        self.y = y

    def _manhattan_distance(self, point):
        raise NotImplemented("Not implemented yet.")

    def _euclidean_distance(self, point):
        raise NotImplemented("Not implemented yet.")

    def _minkowski_distance(self, point):
        raise NotImplemented("Not implemented yet.")

    def _uniform_weights(self, distances):
        raise NotImplemented("Not implemented yet.")

    def _distance_weights(self, distances):
        raise NotImplemented("Not implemented yet.")

    def _predict_point(self, point):
        raise NotImplemented("Not implemented yet.")

    def predict(self, x):
        raise NotImplemented("Not implemented yet.")